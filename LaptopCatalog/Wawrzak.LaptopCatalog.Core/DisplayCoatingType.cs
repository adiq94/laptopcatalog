﻿namespace Wawrzak.LaptopCatalog.Core {
    public enum DisplayCoatingType {
        Glossy,
        Matte
    }
}