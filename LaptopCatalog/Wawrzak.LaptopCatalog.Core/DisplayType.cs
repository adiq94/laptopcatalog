﻿namespace Wawrzak.LaptopCatalog.Core {
    public enum DisplayType {
        IPS,
        TN,
        VA
    }
}