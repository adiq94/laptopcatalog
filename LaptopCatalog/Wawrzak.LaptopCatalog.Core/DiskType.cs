﻿namespace Wawrzak.LaptopCatalog.Core {
    public enum DiskType {
        SSD,
        HDD,
        Hybrid
    }
}