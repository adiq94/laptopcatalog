﻿namespace Wawrzak.LaptopCatalog.Core {
    public enum MemoryType {
        DDR4,
        DDR3
    }
}