﻿namespace Wawrzak.LaptopCatalog.Core {
    public enum SizeUnit {
        MB,
        GB
    }
}