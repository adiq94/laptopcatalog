﻿using ReactiveUI;
using Wawrzak.LaptopCatalog.UI.ViewModels;

namespace Wawrzak.LaptopCatalog.UI.Views {
    /// <summary>
    ///     Interaction logic for Home.xaml
    /// </summary>
    public partial class Home : ReactiveUserControl<HomeViewModel> {
        public Home() {
            InitializeComponent();
        }
    }
}