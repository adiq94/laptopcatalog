﻿using System.Windows.Controls;
using MaterialDesignThemes.Wpf;
using ReactiveUI;
using Wawrzak.LaptopCatalog.Interfaces;
using Wawrzak.LaptopCatalog.UI.ViewModels;

namespace Wawrzak.LaptopCatalog.UI.Views {
    /// <summary>
    ///     Interaction logic for RamView.xaml
    /// </summary>
    public partial class RamView : ReactiveUserControl<LaptopViewModel> {
        public RamView() {
            InitializeComponent();
            //"Slots: {Ram.Count(x => x.Product != null)}/{Ram.Count}, Memory: {Ram.Where(x => x.Product != null).Sum(x => x.Product.Value)}MB"
        }

        public void CombinedDialogOpenedEventHandler(object sender, DialogOpenedEventArgs eventArgs) {
        }

        public void CombinedDialogClosingEventHandler(object sender, DialogClosingEventArgs eventArgs) {
            if (eventArgs.Parameter is RamViewModel) {
                var view = eventArgs.Session.Content as RamSelectView;
                var ram = eventArgs.Parameter as RamViewModel;
                var slot = view.DataContext as SlotViewModel<IRam>;
                slot.Product = ram;
                var vm = DataContext as LaptopViewModel;
                vm.RaisePropertyChanged(nameof(vm.Ram));
            }
        }


        private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            var vm = DataContext as LaptopViewModel;
            if (vm == null) return;
            if (e.AddedItems.Count != 0) vm.SelectedItem = e.AddedItems[0];
        }
    }
}