﻿using System.Windows.Controls;
using ReactiveUI;
using Wawrzak.LaptopCatalog.UI.ViewModels;

namespace Wawrzak.LaptopCatalog.UI.Views {
    /// <summary>
    ///     Interaction logic for LaptopView.xaml
    /// </summary>
    public partial class LaptopMainView : ReactiveUserControl<LaptopMainViewModel> {
        public LaptopMainView() {
            InitializeComponent();
            ViewModel = new LaptopMainViewModel();
            DataContext = ViewModel;
        }

        private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            var vm = DataContext as LaptopMainViewModel;
            if (vm == null) return;
            if (e.AddedItems.Count != 0) vm.SelectedItem = e.AddedItems[0];
        }
    }
}