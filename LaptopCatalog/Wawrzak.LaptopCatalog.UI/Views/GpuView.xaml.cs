﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MaterialDesignThemes.Wpf;
using ReactiveUI;
using Wawrzak.LaptopCatalog.Interfaces;
using Wawrzak.LaptopCatalog.UI.ViewModels;

namespace Wawrzak.LaptopCatalog.UI.Views
{
    /// <summary>
    /// Interaction logic for GpuView.xaml
    /// </summary>
    public partial class GpuView : ReactiveUserControl<LaptopViewModel> {
        public GpuView()
        {
            InitializeComponent();
        }

        public void CombinedDialogOpenedEventHandler(object sender, DialogOpenedEventArgs eventArgs)
        {
        }

        public void CombinedDialogClosingEventHandler(object sender, DialogClosingEventArgs eventArgs)
        {
            if (eventArgs.Parameter is GpuViewModel)
            {
                var view = eventArgs.Session.Content as GpuSelectView;
                var gpu = eventArgs.Parameter as GpuViewModel;
                var slot = view.DataContext as SlotViewModel<IGpu>;
                slot.Product = gpu;
                var vm = DataContext as LaptopViewModel;
                vm.RaisePropertyChanged(nameof(vm.Gpu));
            }
        }


        private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var vm = DataContext as LaptopViewModel;
            if (vm == null) return;
            if (e.AddedItems.Count != 0) vm.SelectedItem = e.AddedItems[0];
        }
    }
}
