﻿using ReactiveUI;
using Wawrzak.LaptopCatalog.UI.ViewModels;

namespace Wawrzak.LaptopCatalog.UI.Views {
    /// <summary>
    ///     Interaction logic for DisplayView.xaml
    /// </summary>
    public partial class DisplayView : ReactiveUserControl<LaptopViewModel> {
        public DisplayView() {
            ViewModel = (LaptopViewModel)DataContext;
            InitializeComponent();

        }
    }
}