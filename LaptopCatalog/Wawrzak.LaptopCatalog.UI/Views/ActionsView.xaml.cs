﻿using ReactiveUI;
using Wawrzak.LaptopCatalog.UI.ViewModels;

namespace Wawrzak.LaptopCatalog.UI.Views {
    /// <summary>
    ///     Interaction logic for ActionsView.xaml
    /// </summary>
    public partial class ActionsView : ReactiveUserControl<LaptopViewModel> {
        public ActionsView() {
            InitializeComponent();
        }
    }
}