﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ReactiveUI;
using Wawrzak.LaptopCatalog.UI.ViewModels;

namespace Wawrzak.LaptopCatalog.UI.Views
{
    /// <summary>
    /// Interaction logic for ProducerMainView.xaml
    /// </summary>
    public partial class ProducerMainView : ReactiveUserControl<ProducerMainViewModel> {
        public ProducerMainView()
        {
            InitializeComponent();
            ViewModel = new ProducerMainViewModel();
            DataContext = ViewModel;
        }

        private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            var vm = DataContext as ProducerMainViewModel;
            if (vm == null) return;
            if (e.AddedItems.Count != 0) vm.SelectedItem = e.AddedItems[0];
        }
    }
}
