﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MaterialDesignThemes.Wpf;
using ReactiveUI;
using Wawrzak.LaptopCatalog.UI.ViewModels;

namespace Wawrzak.LaptopCatalog.UI.Views
{
    /// <summary>
    /// Interaction logic for ChipsetView.xaml
    /// </summary>
    public partial class ChipsetView : ReactiveUserControl<LaptopViewModel> {
        public ChipsetView()
        {
            InitializeComponent();
        }
        public void CombinedDialogOpenedEventHandler(object sender, DialogOpenedEventArgs eventArgs)
        {
        }

        public void CombinedDialogClosingEventHandler(object sender, DialogClosingEventArgs eventArgs)
        {
            if (eventArgs.Parameter is ChipsetViewModel)
            {
                var view = eventArgs.Session.Content as ChipsetSelectView;
                var chipset = eventArgs.Parameter as ChipsetViewModel;
                if (view.DataContext is LaptopViewModel)
                {
                    var laptop = view.DataContext as LaptopViewModel;
                    laptop.Chipset = chipset;
                }
            }
        }
    }
}
