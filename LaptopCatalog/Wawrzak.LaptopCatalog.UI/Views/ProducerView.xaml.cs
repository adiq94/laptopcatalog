﻿using MaterialDesignThemes.Wpf;
using ReactiveUI;
using Wawrzak.LaptopCatalog.UI.ViewModels;

namespace Wawrzak.LaptopCatalog.UI.Views {
    /// <summary>
    ///     Interaction logic for ProducerView.xaml
    /// </summary>
    public partial class ProducerView : ReactiveUserControl<LaptopViewModel> {
        public ProducerView() {
            InitializeComponent();
        }

        public void CombinedDialogOpenedEventHandler(object sender, DialogOpenedEventArgs eventArgs) {
        }

        public void CombinedDialogClosingEventHandler(object sender, DialogClosingEventArgs eventArgs) {
            if (eventArgs.Parameter is ProducerViewModel) {
                var view = eventArgs.Session.Content as ProducerSelectView;
                var producer = eventArgs.Parameter as ProducerViewModel;
                if (view.DataContext is LaptopViewModel) {
                    var laptop = view.DataContext as LaptopViewModel;

                    var oldLaptop = new LaptopViewModel {
                        Name = laptop.Name,
                        Producer = laptop.Producer
                    };
                    laptop.Producer = producer;
                }
            }
        }
    }
}