﻿using ReactiveUI;
using Wawrzak.LaptopCatalog.UI.ViewModels;

namespace Wawrzak.LaptopCatalog.UI.Views {
    /// <summary>
    ///     Interaction logic for ProducerSelectView.xaml
    /// </summary>
    public partial class RamSelectView : ReactiveUserControl<LaptopViewModel> {
        public RamSelectView() {
            InitializeComponent();
        }
    }
}