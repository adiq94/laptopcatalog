﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wawrzak.LaptopCatalog.UI.Extensions
{
    public static class StringExtensions
    {
        public static bool Contains(this string source, string toCheck, StringComparison comp) {
            if (toCheck == null) return true;
            return source?.IndexOf(toCheck, comp) >= 0;
        }
    }
}
