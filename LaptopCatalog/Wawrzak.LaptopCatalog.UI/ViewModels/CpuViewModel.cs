﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.UI.ViewModels {
    public class CpuViewModel : ReactiveObject, ICpu {
        [Reactive] public IProducer Producer { get; set; }
        [Reactive] public string Name { get; set; }
        [Reactive] public decimal Frequency { get; set; }
    }
}