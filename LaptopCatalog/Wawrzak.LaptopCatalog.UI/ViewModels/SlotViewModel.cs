﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.UI.ViewModels {
    public class SlotViewModel<T> : ReactiveObject, ISlot<T> where T : IProduct {
        [Reactive]
        public T Product { get; set; }

        public override string ToString() => Product?.ToString() ?? "";
    }
}