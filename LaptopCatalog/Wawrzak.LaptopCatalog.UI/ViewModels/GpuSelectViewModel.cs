﻿using System.Collections.ObjectModel;
using System.Linq;
using ReactiveUI;
using Splat;
using Wawrzak.LaptopCatalog.BL;
using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.UI.ViewModels {
    public class GpuSelectViewModel : ReactiveObject {
        public GpuSelectViewModel() {
            var service =
                (BaseProductService<IGpu>) Locator.CurrentMutable.GetService(typeof(BaseProductService<IGpu>));
            Gpus = new ObservableCollection<GpuViewModel>(service.Get().Select(x => new GpuViewModel
            {
                Name = x.Name,
                Frequency = x.Frequency,
                Producer = new ProducerViewModel {
                    Name = x.Producer.Name
                },
                Memory = new MemoryViewModel {
                    Frequency = x.Memory.Frequency,
                    Name = x.Memory.Name,
                    Producer = new ProducerViewModel {
                        Name = x.Producer.Name
                    },
                    Unit = x.Memory.Unit,
                    Value = x.Memory.Value
                }
            }));
        }


        public ObservableCollection<GpuViewModel> Gpus { get; set; }
    }
}