﻿using System.Collections.Generic;
using ReactiveUI;
using Splat;
using Wawrzak.LaptopCatalog.BL;
using Wawrzak.LaptopCatalog.Core;
using Wawrzak.LaptopCatalog.Interfaces;
using Wawrzak.LaptopCatalog.UI.Views;

namespace Wawrzak.LaptopCatalog.UI.ViewModels {
    public class MainWindowViewModel : ReactiveObject {
        public MainWindowViewModel() {
            Seed();
        }

        public void Seed() {
            var chipsetService =
                (BaseProductService<IChipset>)Locator.CurrentMutable.GetService(typeof(BaseProductService<IChipset>));
            var cpuService =
                (BaseProductService<ICpu>)Locator.CurrentMutable.GetService(typeof(BaseProductService<ICpu>));
            var diskService =
                (BaseProductService<IDisk>)Locator.CurrentMutable.GetService(typeof(BaseProductService<IDisk>));
            var displayService =
                (BaseService<IDisplay>)Locator.CurrentMutable.GetService(typeof(BaseService<IDisplay>));
            var gpuService =
                (BaseProductService<IGpu>)Locator.CurrentMutable.GetService(typeof(BaseProductService<IGpu>));
            var laptopService =
                (BaseProductService<ILaptop>)Locator.CurrentMutable.GetService(typeof(BaseProductService<ILaptop>));
            var osService =
                (BaseProductService<IOperatingSystem>)Locator.CurrentMutable.GetService(
                    typeof(BaseProductService<IOperatingSystem>));
            var producerService =
                (BaseService<IProducer>)Locator.CurrentMutable.GetService(typeof(BaseService<IProducer>));
            var ramService =
                (BaseProductService<IRam>)Locator.CurrentMutable.GetService(typeof(BaseProductService<IRam>));

            var producer = new ProducerViewModel { Name = "MSI" };
            var producer2 = new ProducerViewModel { Name = "Dell" };
            var prodcuer3 = new ProducerViewModel { Name = "Nieznany" };
            var producer4 = new ProducerViewModel { Name = "Intel" };
            var producer5 = new ProducerViewModel {Name = "Razer"};
            var producer6 = new ProducerViewModel {Name = "HP"};
            var producer7 = new ProducerViewModel {Name = "Nvidia"};
            var producer8 = new ProducerViewModel {Name = "Microsoft"};
            var chipset = new ChipsetViewModel { Name = "Zintegrowany", Producer = prodcuer3 };
            var chipset2 = new ChipsetViewModel {Name = "Intel", Producer = producer4};
            var ram = new RamViewModel
            { Frequency = 1866, Name = "Ram 1866", Producer = prodcuer3, Unit = SizeUnit.MB, Value = 1024 * 16 };
            var ram3 = new RamViewModel
                { Frequency = 2133, Name = "Ram 2133", Producer = prodcuer3, Unit = SizeUnit.MB, Value = 1024 * 16 };
            var ram4 = new RamViewModel
                { Frequency = 1600, Name = "Ram 1600", Producer = prodcuer3, Unit = SizeUnit.MB, Value = 1024 * 8 };
            var ram5 = new RamViewModel
                { Frequency = 2666, Name = "Ram 2666", Producer = prodcuer3, Unit = SizeUnit.MB, Value = 1024 * 16 };
            var cpu2 = new CpuViewModel {Frequency = 4000, Name = "i7-8550U", Producer = producer4};
            var cpu3 = new CpuViewModel {Frequency = 2000, Name = "i3-5005U", Producer = producer4};
            var cpu4 = new CpuViewModel {Frequency = 4800, Name = "i9-8950HK", Producer = producer4};
            var disk = new DiskViewModel
            { Name = "Disk", Producer = producer, Type = DiskType.HDD, Unit = SizeUnit.GB, Value = 500 };
            var disk2 = new DiskViewModel
            { Name = "SSD 512 M.2 PCIe", Producer = prodcuer3, Type = DiskType.SSD, Unit = SizeUnit.GB, Value = 512 };
            var disk3 = new DiskViewModel
                { Name = "SSD 256 M.2 PCIe", Producer = prodcuer3, Type = DiskType.SSD, Unit = SizeUnit.GB, Value = 256 };
            var disk4 = new DiskViewModel
                { Name = "SSD 256 SATA", Producer = prodcuer3, Type = DiskType.SSD, Unit = SizeUnit.GB, Value = 256 };
            var disk5 = new DiskViewModel {
                Name = "SSD 1000 M.2 PCIe",
                Producer = prodcuer3,
                Type = DiskType.SSD,
                Unit = SizeUnit.GB,
                Value = 1000
            };
            var disk6 = new DiskViewModel
            {
                Name = "HDD 1000 SATA 7200 obr.",
                Producer = prodcuer3,
                Type = DiskType.HDD,
                Unit = SizeUnit.GB,
                Value = 1000
            };

            var display2 = new DisplayViewModel
            {
                CoatingType = DisplayCoatingType.Glossy,
                Type = DisplayType.IPS,
                ResolutionHorizontal = 3200,
                ResolutionVertical = 1800,
                Size = 13.3M
            };
            var display = new DisplayViewModel
            {
                CoatingType = DisplayCoatingType.Matte,
                ResolutionHorizontal = 1920,
                ResolutionVertical = 1080,
                Size = 14,
                Type = DisplayType.IPS
            };
            var display3 = new DisplayViewModel {
                CoatingType = DisplayCoatingType.Matte,
                ResolutionHorizontal = 1366,
                ResolutionVertical = 768,
                Size = 15.6M,
                Type = DisplayType.TN
            };
            var display4 = new DisplayViewModel
            {
                CoatingType = DisplayCoatingType.Matte,
                ResolutionHorizontal = 3840,
                ResolutionVertical = 2160,
                Size = 15.6M,
                Type = DisplayType.IPS
            };
            var gpu2 = new GpuViewModel
            {
                Frequency = 1150,
                Memory = new MemoryViewModel
                {
                    Frequency = 0,
                    Name = "Zintegrowana UHD Graphics 620",
                    Producer = prodcuer3,
                    Unit = SizeUnit.MB,
                    Value = 0
                },
                Name = "UHD Graphics 620",
                Producer = producer4
            };
            var gpu3 = new GpuViewModel {
                Name = "HD 5500",
                Frequency = 950,
                Memory = new MemoryViewModel {
                    Frequency = 0,
                    Name = "Zintegrowana HD 5500",
                    Producer = prodcuer3,
                    Unit = SizeUnit.MB,
                    Value = 0
                },
                Producer = producer4
            };
            var gpu4 = new GpuViewModel {
                Name = "GTX 1080 Max-Q",
                Frequency = 2000,
                Memory = new MemoryViewModel {
                    Frequency = 10000,
                    Name = "Pamięć 10000Mhz",
                    Producer = prodcuer3,
                    Unit = SizeUnit.MB,
                    Value = 8192
                },
                Producer = producer7
            };
            var os = new OperatingSystemViewModel { Name = "Windows 10 Pro PL", Producer = producer8 };
            var os2 = new OperatingSystemViewModel { Name = "Windows 7", Producer = producer8 };
            var os3 = new OperatingSystemViewModel { Name = "Linux", Producer = new ProducerViewModel{ Name = "Open Source"} };
            var os4 = new OperatingSystemViewModel { Name = "Windows 10 Home PL", Producer = producer8 };
            var xps13 = new LaptopViewModel {
                Chipset = chipset,
                Cpu = cpu2,
                Currency = Currency.PLN,
                Disk = new List<ISlot<IDisk>> {new SlotViewModel<IDisk> {Product = disk2}},
                Display = display2,
                Gpu = new List<ISlot<IGpu>> {new SlotViewModel<IGpu> {Product = gpu2}},
                Name = "XPS 13 9360",
                OperatingSystem = os,
                Producer = producer2,
                Price = 8099.00M,
                Ram = new List<ISlot<IRam>> { new SlotViewModel<IRam> { Product = ram3 } }
            };
            var razerBlade = new LaptopViewModel {
                Chipset = chipset,
                Cpu = cpu2,
                Currency = Currency.PLN,
                Disk = new List<ISlot<IDisk>> {new SlotViewModel<IDisk> {Product = disk3}},
                Display = display2,
                Gpu = new List<ISlot<IGpu>> {new SlotViewModel<IGpu> {Product = gpu2}},
                Name = "Blade Stealth",
                OperatingSystem = os4,
                Producer = producer5,
                Price = 7599.00M,
                Ram = new List<ISlot<IRam>> {new SlotViewModel<IRam> {Product = ram3}}
            };
            var hpbs150nw = new LaptopViewModel {
                Chipset = chipset2,
                Cpu = cpu3,
                Currency = Currency.PLN,
                Disk = new List<ISlot<IDisk>> {new SlotViewModel<IDisk> {Product = disk4}},
                Display = display3,
                Gpu = new List<ISlot<IGpu>> {new SlotViewModel<IGpu> {Product = gpu3}},
                Name = "bs150nw",
                OperatingSystem = os4,
                Producer = producer6,
                Price = 1919.00M,
                Ram = new List<ISlot<IRam>> {new SlotViewModel<IRam> {Product = ram4}, new SlotViewModel<IRam>()}
            };
            var alienware = new LaptopViewModel {
                Name = "Alienware",
                Producer = producer2,
                Chipset = chipset,
                Cpu = cpu4,
                Currency = Currency.PLN,
                Disk = new List<ISlot<IDisk>>
                    {new SlotViewModel<IDisk> {Product = disk5}, new SlotViewModel<IDisk> {Product = disk6}},
                Display = display4,
                Gpu = new List<ISlot<IGpu>> {new SlotViewModel<IGpu> {Product = gpu4}},
                OperatingSystem = os4,
                Price = 17999.00M,
                Ram = new List<ISlot<IRam>>
                    {new SlotViewModel<IRam> {Product = ram5}, new SlotViewModel<IRam> {Product = ram5}}
            };

            chipsetService.Add(chipset);
            chipsetService.Add(chipset2);
            cpuService.Add(cpu2);
            cpuService.Add(cpu3);
            cpuService.Add(cpu4);
            diskService.Add(disk);
            diskService.Add(disk2);
            diskService.Add(disk3);
            diskService.Add(disk4);
            diskService.Add(disk5);
            diskService.Add(disk6);
            gpuService.Add(gpu2);
            gpuService.Add(gpu3);
            gpuService.Add(gpu4);
            laptopService.Add(xps13);
            laptopService.Add(razerBlade);
            laptopService.Add(hpbs150nw);
            laptopService.Add(alienware);
            osService.Add(os);
            osService.Add(os2);
            osService.Add(os3);
            osService.Add(os4);
            producerService.Add(producer);
            producerService.Add(producer2);
            producerService.Add(prodcuer3);
            producerService.Add(producer4);
            producerService.Add(producer5);
            producerService.Add(producer6);
            producerService.Add(producer7);
            ramService.Add(ram);
            ramService.Add(ram3);
            ramService.Add(ram4);
        }

        public MenuItem[] MenuItems { get; } = {
            new MenuItem("Laptops", new LaptopMainView()),
            new MenuItem("Producers", new ProducerMainView())
        };
    }
}