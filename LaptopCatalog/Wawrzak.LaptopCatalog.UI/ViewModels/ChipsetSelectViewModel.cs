﻿using System.Collections.ObjectModel;
using System.Linq;
using ReactiveUI;
using Splat;
using Wawrzak.LaptopCatalog.BL;
using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.UI.ViewModels {
    public class ChipsetSelectViewModel : ReactiveObject {
        public ChipsetSelectViewModel() {
            var service =
                (BaseProductService<IChipset>) Locator.CurrentMutable.GetService(typeof(BaseProductService<IChipset>));
            Chipsets = new ObservableCollection<ChipsetViewModel>(service.Get().Select(x =>
                new ChipsetViewModel
                {
                    Name = x.Name,
                    Producer = new ProducerViewModel {
                        Name = x.Producer.Name
                    }
                }));
        }


        public ObservableCollection<ChipsetViewModel> Chipsets { get; set; }
    }
}