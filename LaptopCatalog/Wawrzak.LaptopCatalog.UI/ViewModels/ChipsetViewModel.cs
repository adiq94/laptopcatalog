﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.UI.ViewModels {
    public class ChipsetViewModel : ReactiveObject, IChipset {
        [Reactive] public IProducer Producer { get; set; }
        [Reactive] public string Name { get; set; }
    }
}