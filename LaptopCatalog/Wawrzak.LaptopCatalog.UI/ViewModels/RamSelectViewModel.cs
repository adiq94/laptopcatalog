﻿using System.Collections.ObjectModel;
using System.Linq;
using ReactiveUI;
using Splat;
using Wawrzak.LaptopCatalog.BL;
using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.UI.ViewModels {
    public class RamSelectViewModel : ReactiveObject {
        public RamSelectViewModel() {
            var ramService =
                (BaseProductService<IRam>) Locator.CurrentMutable.GetService(typeof(BaseProductService<IRam>));
            Rams = new ObservableCollection<RamViewModel>(ramService.Get().Select(x => new RamViewModel {
                Name = x.Name,
                Frequency = x.Frequency,
                Producer = new ProducerViewModel {
                    Name = x.Producer.Name
                },
                Unit = x.Unit,
                Value = x.Value
            }));
        }


        public ObservableCollection<RamViewModel> Rams { get; set; }
    }
}