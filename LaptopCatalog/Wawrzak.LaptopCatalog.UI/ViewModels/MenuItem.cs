﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using Wawrzak.LaptopCatalog.UI.Properties;

namespace Wawrzak.LaptopCatalog.UI.ViewModels {
    public class MenuItem : INotifyPropertyChanged {
        private object _content;
        private ScrollBarVisibility _horizontalScrollBarVisibilityRequirement;
        private Thickness _marginRequirement = new Thickness(16);

        private string _name;
        private ScrollBarVisibility _verticalScrollBarVisibilityRequirement;

        public MenuItem(string name, object content) {
            Name = name;
            Content = content;
        }

        public string Name {
            get => _name;
            set {
                _name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        public object Content {
            get => _content;
            set {
                _content = value;
                OnPropertyChanged(nameof(Content));
            }
        }

        public ScrollBarVisibility HorizontalScrollBarVisibilityRequirement {
            get => _horizontalScrollBarVisibilityRequirement;
            set {
                _horizontalScrollBarVisibilityRequirement = value;
                OnPropertyChanged(nameof(HorizontalScrollBarVisibilityRequirement));
            }
        }

        public ScrollBarVisibility VerticalScrollBarVisibilityRequirement {
            get => _verticalScrollBarVisibilityRequirement;
            set {
                _verticalScrollBarVisibilityRequirement = value;
                OnPropertyChanged(nameof(VerticalScrollBarVisibilityRequirement));
            }
        }

        public Thickness MarginRequirement {
            get => _marginRequirement;
            set {
                _marginRequirement = value;
                OnPropertyChanged(nameof(MarginRequirement));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}