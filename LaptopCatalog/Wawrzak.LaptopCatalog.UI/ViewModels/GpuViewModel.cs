﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.UI.ViewModels {
    public class GpuViewModel : ReactiveObject, IGpu {
        [Reactive] public IProducer Producer { get; set; }
        [Reactive] public string Name { get; set; }
        [Reactive] public decimal Frequency { get; set; }
        [Reactive] public IMemory Memory { get; set; }

        public string FrequencyFormatted => $"{Frequency} MHz";
    }
}