﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Linq;
using DynamicData;
using DynamicData.Binding;
using MaterialDesignThemes.Wpf;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Wawrzak.LaptopCatalog.Core;
using Wawrzak.LaptopCatalog.Interfaces;
using Wawrzak.LaptopCatalog.UI.Commands;
using Wawrzak.LaptopCatalog.UI.Extensions;

namespace Wawrzak.LaptopCatalog.UI.ViewModels {
    public class LaptopViewModel : ReactiveObject, ILaptop {
       

        public LaptopViewModel() {
            AddRamSlotCommand = new Command(_ => {
                Ram.Add(new SlotViewModel<IRam>());
                this.RaisePropertyChanged(nameof(Ram));
            });
            DeleteRamSlotCommand = new Command(_ => {
                if (SelectedItem is ISlot<IRam> item) {
                    Ram.Remove(item);
                    this.RaisePropertyChanged(nameof(Ram));
                }
            });
            AddGpuSlotCommand = new Command(_ => {
                Gpu.Add(new SlotViewModel<IGpu>());
                this.RaisePropertyChanged(nameof(Gpu));
            });
            DeleteGpuSlotCommand = new Command(_ => {
                if (SelectedItem is ISlot<IGpu> item)
                {
                    Gpu.Remove(item);
                    this.RaisePropertyChanged(nameof(Gpu));
                }
            });
            AddDiskSlotCommand = new Command(_ => {
                Disk.Add(new SlotViewModel<IDisk>());
                this.RaisePropertyChanged(nameof(Disk));
            });
            DeleteDiskSlotCommand = new Command(_ => {
                if (SelectedItem is ISlot<IDisk> item)
                {
                    Disk.Remove(item);
                    this.RaisePropertyChanged(nameof(Disk));
                }
            });
            SaveCommand = new Command(x => {
                var vm = x as LaptopMainViewModel;
                try {
                    vm?.LaptopService.Put(this);
                    Dirty = false;
                    //vm?.LoadLaptops();
                }
                catch (ArgumentNullException e) {
                    if (vm != null) vm.Error = e.Message;
                    DrawerHost.OpenDrawerCommand.Execute(null, null);
                }
            });

            this.displaySummary = this.WhenAnyValue(
                    x => x.Display.Size, x => x.Display.ResolutionHorizontal, x => x.Display.ResolutionVertical)
                .Select(x => $"{x?.Item1}\" {x?.Item2}x{x?.Item3}")
                .ToProperty(this, x => x.DisplaySummary);
            this.WhenAnyValue(
                x => x.Display.CoatingType,
                x => x.Display.ResolutionHorizontal,
                x => x.Display.ResolutionVertical,
                x => x.Display.Size,
                x => x.Display.Type).InvokeCommand(new Command(_ => Dirty = true));
            this.PropertyChanged += OnPropertyChanged;
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            if(e.PropertyName != nameof(Dirty) && e.PropertyName != nameof(IsExpanded))
                Dirty = true;
            if (e.PropertyName == nameof(Ram)) this.RaisePropertyChanged(nameof(RamSummary));
            if (e.PropertyName == nameof(Gpu)) this.RaisePropertyChanged(nameof(GpuSummary));
            if (e.PropertyName == nameof(Disk)) this.RaisePropertyChanged(nameof(DiskSummary));
        }


        readonly ObservableAsPropertyHelper<string> displaySummary;
        public string DisplaySummary => displaySummary.Value;

        [Reactive]
        public bool Dirty { get; set; }
        [Reactive]
        public bool IsExpanded { get; set; }

        public string RamSummary =>
            $"Sloty: {Ram.Count(x => x.Product != null)}/{Ram.Count}, Rozmiar: {Ram.Where(x => x.Product != null).Sum(x => x.Product.Value)}MB";

        public string GpuSummary => $"Sloty: {Gpu.Count(x => x.Product != null)}/{Gpu.Count}";

        public string DiskSummary => $"Sloty: {Disk.Count(x => x.Product != null)}/{Disk.Count}, Rozmiar: {Disk.Where(x => x.Product != null).Sum(x => x.Product.Value)}GB";


        public Command AddRamSlotCommand { get; }
        public object SelectedItem { get; set; }
        public Command DeleteRamSlotCommand { get; }
        public Command SaveCommand { get; }
        public Command AddGpuSlotCommand { get; }
        public Command DeleteGpuSlotCommand { get; }
        public Command AddDiskSlotCommand { get; }
        public Command DeleteDiskSlotCommand { get; }

        [Reactive]
        public IProducer Producer { get; set; }

        [Reactive]
        public string Name { get; set; }

        [Reactive]
        public decimal Price { get; set; }

        [Reactive]
        public Currency Currency { get; set; }

        [Reactive]
        public IDisplay Display { get; set; } = new DisplayViewModel();

        [Reactive]
        public ICpu Cpu { get; set; }

        public ICollection<ISlot<IRam>> Ram { get; set; } = new ObservableCollection<ISlot<IRam>>();

        public ICollection<ISlot<IGpu>> Gpu { get; set; } = new ObservableCollection<ISlot<IGpu>>();
 
        public ICollection<ISlot<IDisk>> Disk { get; set; } = new ObservableCollection<ISlot<IDisk>>();

        [Reactive]
        public IChipset Chipset { get; set; }

        [Reactive]
        public IOperatingSystem OperatingSystem { get; set; }
    }
}