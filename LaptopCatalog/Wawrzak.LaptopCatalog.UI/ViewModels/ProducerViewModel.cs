﻿using System;
using MaterialDesignThemes.Wpf;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Wawrzak.LaptopCatalog.Interfaces;
using Wawrzak.LaptopCatalog.UI.Commands;

namespace Wawrzak.LaptopCatalog.UI.ViewModels {
    public class ProducerViewModel : ReactiveObject, IProducer {
        [Reactive] public string Name { get; set; }
        [Reactive] public bool Dirty { get; set; }
        public Command SaveCommand { get; }

        public ProducerViewModel() {
            SaveCommand = new Command(x => {
                var vm = x as ProducerMainViewModel;
                try
                {
                    vm?.ProducerService.Put(this);
                    Dirty = false;
                    //vm?.LoadLaptops();
                }
                catch (ArgumentNullException e)
                {
                    if (vm != null) vm.Error = e.Message;
                    DrawerHost.OpenDrawerCommand.Execute(null, null);
                }
            });
            this.WhenAnyValue(
                x => x.Name).InvokeCommand(new Command(_ => Dirty = true));
        }
    }
}