﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Wawrzak.LaptopCatalog.Core;
using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.UI.ViewModels {
    public class MemoryViewModel : ReactiveObject, IMemory {
        [Reactive] public decimal Frequency { get; set; }
        [Reactive] public decimal Value { get; set; }
        [Reactive] public SizeUnit Unit { get; set; }
        [Reactive] public IProducer Producer { get; set; }
        [Reactive] public string Name { get; set; }

        public string FrequencyFormatted => $"{Frequency} MHz";
        public string SizeFormatted => $"{Value} {Unit}";
    }
}