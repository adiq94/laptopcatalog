﻿using System;
using System.Collections.Generic;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Wawrzak.LaptopCatalog.Core;
using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.UI.ViewModels {
    public class DisplayViewModel : ReactiveObject, IDisplay {
        [Reactive] public DisplayType Type { get; set; }
        [Reactive] public DisplayCoatingType CoatingType { get; set; }
        [Reactive] public decimal Size { get; set; }
        [Reactive] public int ResolutionHorizontal { get; set; }
        [Reactive] public int ResolutionVertical { get; set; }

        public List<string> DisplayTypes => new List<string> {
            Enum.GetName(typeof(DisplayType), DisplayType.IPS),
            Enum.GetName(typeof(DisplayType), DisplayType.TN),
            Enum.GetName(typeof(DisplayType), DisplayType.VA),
        };

        public List<string> DisplayCoatingTypes => new List<string> {
            "Błyszcząca",
            "Matowa"
        };

    }
}