﻿using System.Collections.ObjectModel;
using System.Linq;
using ReactiveUI;
using Splat;
using Wawrzak.LaptopCatalog.BL;
using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.UI.ViewModels {
    public class ProducerSelectViewModel : ReactiveObject {
        public ProducerSelectViewModel() {
            var producerService =
                (BaseService<IProducer>) Locator.CurrentMutable.GetService(typeof(BaseService<IProducer>));
            Producers = new ObservableCollection<ProducerViewModel>(producerService.Get().Select(x =>
                new ProducerViewModel {
                    Name = x.Name
                }));
        }


        public ObservableCollection<ProducerViewModel> Producers { get; set; }
    }
}