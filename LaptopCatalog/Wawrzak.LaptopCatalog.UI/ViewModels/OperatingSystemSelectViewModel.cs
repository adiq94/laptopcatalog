﻿using System.Collections.ObjectModel;
using System.Linq;
using ReactiveUI;
using Splat;
using Wawrzak.LaptopCatalog.BL;
using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.UI.ViewModels {
    public class OperatingSystemSelectViewModel : ReactiveObject {
        public OperatingSystemSelectViewModel()
        {
            var service =
                (BaseProductService<IOperatingSystem>)Locator.CurrentMutable.GetService(typeof(BaseProductService<IOperatingSystem>));
            OSes = new ObservableCollection<OperatingSystemViewModel>(service.Get().Select(x =>
                new OperatingSystemViewModel
                {
                    Name = x.Name,
                    Producer = new ProducerViewModel {
                        Name = x.Producer.Name
                    }
                }));
        }


        public ObservableCollection<OperatingSystemViewModel> OSes { get; set; }
    }
}