﻿using System.Collections.ObjectModel;
using System.Linq;
using ReactiveUI;
using Splat;
using Wawrzak.LaptopCatalog.BL;
using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.UI.ViewModels {
    public class DiskSelectViewModel : ReactiveObject {
        public DiskSelectViewModel() {
            var service =
                (BaseProductService<IDisk>) Locator.CurrentMutable.GetService(typeof(BaseProductService<IDisk>));
            Disks = new ObservableCollection<DiskViewModel>(service.Get().Select(x => new DiskViewModel
            {
                Name = x.Name,
                Producer = new ProducerViewModel {
                    Name = x.Producer.Name
                },
                Unit = x.Unit,
                Value = x.Value,
                Type = x.Type
            }));
        }


        public ObservableCollection<DiskViewModel> Disks { get; set; }
    }
}