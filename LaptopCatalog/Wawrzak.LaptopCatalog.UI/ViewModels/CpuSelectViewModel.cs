﻿using System.Collections.ObjectModel;
using System.Linq;
using ReactiveUI;
using Splat;
using Wawrzak.LaptopCatalog.BL;
using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.UI.ViewModels {
    public class CpuSelectViewModel : ReactiveObject {
        public CpuSelectViewModel() {
            var service =
                (BaseProductService<ICpu>) Locator.CurrentMutable.GetService(typeof(BaseProductService<ICpu>));
            Cpus = new ObservableCollection<CpuViewModel>(service.Get().Select(x =>
                new CpuViewModel
                {
                    Name = x.Name,
                    Frequency = x.Frequency,
                    Producer = new ProducerViewModel {
                        Name = x.Producer.Name
                    }
                }));
        }


        public ObservableCollection<CpuViewModel> Cpus { get; set; }
    }
}