﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;
using Wawrzak.LaptopCatalog.BL;
using Wawrzak.LaptopCatalog.Interfaces;
using Wawrzak.LaptopCatalog.UI.Commands;
using Wawrzak.LaptopCatalog.UI.Extensions;

namespace Wawrzak.LaptopCatalog.UI.ViewModels
{
    public class ProducerMainViewModel : ReactiveObject
    {
        public BaseService<IProducer> ProducerService { get; }
        public ObservableCollection<ProducerViewModel> Producers { get; set; }

        public ProducerMainViewModel() {
            AddProducerCommand = new Command(_ => {
                Producers.Add(new ProducerViewModel
                {
                    Dirty = true
                });
            });
            DeleteProducerCommand = new Command(_ => {
                if (SelectedItem is ProducerViewModel item)
                {
                    Producers.Remove(item);
                    ProducerService.Delete(item);
                }
            });
            ProducerService =
                (BaseService<IProducer>)Locator.CurrentMutable.GetService(typeof(BaseService<IProducer>));
            Producers = new ObservableCollection<ProducerViewModel>(ProducerService.Get().Select(x =>
                new ProducerViewModel
                {
                    Name = x.Name
                }));

            this
                .WhenAnyValue(x => x.SearchText)
                .Throttle(TimeSpan.FromMilliseconds(300))
                .Select(term => term?.Trim())
                .DistinctUntilChanged()
                .Where(term => term != null)
                .Subscribe(FilterProducersCommand);

            SearchResults = Producers;
            Producers.CollectionChanged += (sender, args) => FilterProducersCommand(SearchText);
        }

        private void FilterProducersCommand(string searchText) {
            SearchResults = SearchProduct(searchText);
        }

        private ICollection<ProducerViewModel> SearchProduct(string arg) {
            if (string.IsNullOrEmpty(arg)) return Producers;
            return Producers.Where(x => x.Name.Contains(arg, StringComparison.InvariantCultureIgnoreCase)).ToList();
        }

        public Command AddProducerCommand { get; }
        public object SelectedItem { get; set; }
        public Command DeleteProducerCommand { get; }

        [Reactive]
        public string Error { get; set; }

        [Reactive]
        public string SearchText { get; set; }

        public ICollection<ProducerViewModel> SearchResults { get; set; }
    }
}
