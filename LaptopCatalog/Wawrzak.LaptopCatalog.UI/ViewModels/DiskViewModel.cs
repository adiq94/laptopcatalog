﻿using ReactiveUI.Fody.Helpers;
using Wawrzak.LaptopCatalog.Core;
using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.UI.ViewModels {
    public class DiskViewModel : IDisk {
        [Reactive] public IProducer Producer { get; set; }
        [Reactive] public string Name { get; set; }
        [Reactive] public decimal Value { get; set; }
        [Reactive] public SizeUnit Unit { get; set; }
        [Reactive] public DiskType Type { get; set; }

        public string SizeFormatted => $"{Value} {Unit}";
    }
}