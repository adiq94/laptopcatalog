﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;
using Wawrzak.LaptopCatalog.BL;
using Wawrzak.LaptopCatalog.Core;
using Wawrzak.LaptopCatalog.Interfaces;
using Wawrzak.LaptopCatalog.UI.Commands;
using Wawrzak.LaptopCatalog.UI.Extensions;

namespace Wawrzak.LaptopCatalog.UI.ViewModels {
    public class LaptopMainViewModel : ReactiveObject {
        private readonly BaseService<IProducer> producerService;


        public LaptopMainViewModel() {
            AddLaptopCommand = new Command(_ => {
                Laptops.Add(new LaptopViewModel {
                    Dirty = true
                });
            });
            DeleteLaptopCommand = new Command(_ => {
                if (SelectedItem is LaptopViewModel item) {
                    Laptops.Remove(item);
                    LaptopService.Delete(item);
                }
            });
            LaptopService =
                (BaseProductService<ILaptop>) Locator.CurrentMutable.GetService(typeof(BaseProductService<ILaptop>));
            producerService =
                (BaseService<IProducer>) Locator.CurrentMutable.GetService(typeof(BaseService<IProducer>));
            LoadLaptops();
            Producers = new ObservableCollection<ProducerViewModel>(producerService.Get().Select(x =>
                new ProducerViewModel {
                    Name = x.Name
                }));


            this
                .WhenAnyValue(x => x.SearchText)
                .Throttle(TimeSpan.FromMilliseconds(300))
                .Select(term => term?.Trim())
                .DistinctUntilChanged()
                .Where(term => term != null)
                .Subscribe(FilterLaptopsCommand);

            SearchResults = Laptops;
            Laptops.CollectionChanged += (sender, args) => FilterLaptopsCommand(SearchText);
        }

        public void LoadLaptops() {
            Laptops = new ObservableCollection<LaptopViewModel>(LaptopService.Get().Select(x => new LaptopViewModel {
                Chipset = new ChipsetViewModel {
                    Name = x.Chipset.Name,
                    Producer = new ProducerViewModel {
                        Name = x.Chipset.Producer.Name
                    }
                },
                Cpu = new CpuViewModel {
                    Frequency = x.Cpu.Frequency,
                    Name = x.Cpu.Name,
                    Producer = new ProducerViewModel {
                        Name = x.Cpu.Producer.Name
                    }
                },
                Currency = x.Currency,
                Disk = new ObservableCollection<ISlot<IDisk>>(
                    x.Disk.Where(y => y.Product != null).Select(y => new SlotViewModel<IDisk> {
                        Product = new DiskViewModel {
                            Name = y.Product.Name,
                            Producer = new ProducerViewModel {
                                Name = y.Product.Producer.Name
                            },
                            Unit = y.Product.Unit,
                            Value = y.Product.Value,
                            Type = y.Product.Type
                        }
                    }).Concat(x.Disk.Where(y => y.Product == null).Select(y => new SlotViewModel<IDisk>()))),
                Display = new DisplayViewModel {
                    CoatingType = x.Display.CoatingType,
                    ResolutionHorizontal = x.Display.ResolutionHorizontal,
                    ResolutionVertical = x.Display.ResolutionVertical,
                    Size = x.Display.Size,
                    Type = x.Display.Type
                },
                Gpu = new ObservableCollection<ISlot<IGpu>>(
                    x.Gpu.Where(y => y.Product != null).Select(y => new SlotViewModel<IGpu> {
                        Product = new GpuViewModel() {
                            Name = y.Product.Name,
                            Producer = new ProducerViewModel {
                                Name = y.Product.Producer.Name
                            },
                            Frequency = y.Product.Frequency,
                            Memory = new MemoryViewModel {
                                Frequency = y.Product.Memory.Frequency,
                                Name = y.Product.Memory.Name,
                                Producer = new ProducerViewModel {
                                    Name = y.Product.Memory.Producer.Name
                                },
                                Unit = y.Product.Memory.Unit,
                                Value = y.Product.Memory.Value
                            }
                        }
                    }).Concat(x.Gpu.Where(y => y.Product == null).Select(y => new SlotViewModel<IGpu>()))),
                Name = x.Name,
                OperatingSystem = new OperatingSystemViewModel {
                    Name = x.OperatingSystem.Name,
                    Producer = new ProducerViewModel {
                        Name = x.OperatingSystem.Producer.Name
                    }
                },
                Price = x.Price,
                Producer = new ProducerViewModel {
                    Name = x.Producer.Name
                },
                Ram = new ObservableCollection<ISlot<IRam>>(
                    x.Ram.Where(y => y.Product != null).Select(y => new SlotViewModel<IRam> {
                        Product = new RamViewModel {
                            Name = y.Product.Name,
                            Frequency = y.Product.Frequency,
                            Producer = new ProducerViewModel {
                                Name = y.Product.Producer.Name
                            },
                            Unit = y.Product.Unit,
                            Value = y.Product.Value
                        }
                    }).Concat(x.Ram.Where(y => y.Product == null).Select(y => new SlotViewModel<IRam>()))),
                Dirty = false
            }));
        }

        private void FilterLaptopsCommand(string arg) {
            SearchResults = SearchLaptop(arg);
        }

        private ICollection<LaptopViewModel> SearchLaptop(string arg) {
            if (string.IsNullOrEmpty(arg)) return Laptops;
            return Laptops.Where(x => x.Name.Contains(arg, StringComparison.InvariantCultureIgnoreCase)).ToList();
        }

        public List<string> Currencies => new List<string> {
            Enum.GetName(typeof(Currency), Currency.PLN),
            Enum.GetName(typeof(Currency), Currency.EUR)
        };

        public Command AddLaptopCommand { get; }
        public object SelectedItem { get; set; }
        public Command DeleteLaptopCommand { get; }

        [Reactive]
        public string Error { get; set; }

        public ObservableCollection<LaptopViewModel> Laptops { get; set; }

        public ObservableCollection<ProducerViewModel> Producers { get; set; }

        public BaseProductService<ILaptop> LaptopService { get; }

        [Reactive]
        public string SearchText { get; set; }

        [Reactive]
        public ICollection<LaptopViewModel> SearchResults { get; set; }
    }
}