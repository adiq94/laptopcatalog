﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Wawrzak.LaptopCatalog.Core;

namespace Wawrzak.LaptopCatalog.UI.Converters
{
    public class DiskTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            var selectedEnumvalue = (DiskType)value;

            switch (selectedEnumvalue)
            {
                case DiskType.HDD:
                    return "HDD";
                case DiskType.SSD:
                    return "SSD";
                case DiskType.Hybrid:
                    return "Hybrydowy";
                default:
                    throw new InvalidOperationException();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            var selectedEnumName = (string)value;

            switch (selectedEnumName)
            {
                case "HDD":
                    return DiskType.HDD;
                case "SSD":
                    return DiskType.SSD;
                case "Hybrydowy":
                    return DiskType.Hybrid;
                default:
                    throw new InvalidOperationException();
            }
        }
    }
}
