﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Wawrzak.LaptopCatalog.Core;

namespace Wawrzak.LaptopCatalog.UI.Converters
{
    public class DisplayCoatingTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null) return null;
            var selectedEnumvalue = (DisplayCoatingType)value;

            switch (selectedEnumvalue) {
                case DisplayCoatingType.Glossy:
                    return "Błyszcząca";
                case DisplayCoatingType.Matte:
                    return "Matowa";
                default:
                    throw new InvalidOperationException();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null) return null;
            var selectedEnumName = (string)value;

            switch (selectedEnumName) {
                case "Błyszcząca":
                    return DisplayCoatingType.Glossy;
                case "Matowa":
                    return DisplayCoatingType.Matte;
                default:
                    throw new InvalidOperationException();
            }
        }
    }
}
