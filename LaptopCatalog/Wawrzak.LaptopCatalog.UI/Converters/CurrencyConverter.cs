﻿using System;
using System.Globalization;
using System.Windows.Data;
using Wawrzak.LaptopCatalog.Core;

namespace Wawrzak.LaptopCatalog.UI.Converters {
    public class CurrencyConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null) return null;
            var selectedEnumvalue = (Currency) value;

            var selectedEnumName = Enum.GetName(typeof(Currency), selectedEnumvalue);

            return selectedEnumName;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null) return null;
            var selectedEnumName = (string) value;

            var selectedEnumValue = Enum.Parse(typeof(Currency), selectedEnumName);

            return selectedEnumValue;
        }
    }
}