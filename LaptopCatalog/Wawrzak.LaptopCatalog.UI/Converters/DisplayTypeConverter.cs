﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Wawrzak.LaptopCatalog.Core;

namespace Wawrzak.LaptopCatalog.UI.Converters
{
    public class DisplayTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null) return null;
            var selectedEnumvalue = (DisplayType)value;

            var selectedEnumName = Enum.GetName(typeof(DisplayType), selectedEnumvalue);

            return selectedEnumName;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null) return null;
            var selectedEnumName = (string) value;

            return Enum.Parse(typeof(DisplayType), selectedEnumName);

        }
    }
}
