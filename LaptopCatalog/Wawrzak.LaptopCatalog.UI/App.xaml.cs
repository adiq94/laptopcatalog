﻿using System;
using System.IO;
using System.Reflection;
using System.Windows;
using ReactiveUI;
using Splat;
using Wawrzak.LaptopCatalog.BL;
using Wawrzak.LaptopCatalog.Interfaces;
using Wawrzak.LaptopCatalog.UI.Properties;

namespace Wawrzak.LaptopCatalog.UI {
    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application {
        public App() {
            // A helper method that will register all classes that derive off IViewFor 
            // into our dependency injection container. ReactiveUI uses Splat for it's 
            // dependency injection by default, but you can override this if you like.
            Locator.CurrentMutable.RegisterViewsForViewModels(Assembly.GetCallingAssembly());
            var dll = Assembly.LoadFile(Settings.Default.DaoPath);
            foreach (var type in dll.GetExportedTypes()) RegisterSingleton<IPersist>(type);
            RegisterProductService<ChipsetService, IChipset>();
            RegisterProductService<CpuService, ICpu>();
            RegisterProductService<DiskService, IDisk>();
            RegisterService<DisplayService, IDisplay>();
            RegisterProductService<GpuService, IGpu>();
            RegisterProductService<LaptopService, ILaptop>();
            RegisterProductService<OSService, IOperatingSystem>();
            RegisterService<ProducerService, IProducer>();
            RegisterProductService<RamService, IRam>();
            foreach (var type in dll.GetExportedTypes()) InitializeWarehouse(type);
            var logger = new DebugLogger() { Level = LogLevel.Debug };
            Locator.CurrentMutable.RegisterConstant(logger, typeof(ILogger));

        }

        private void InitializeWarehouse(Type type) {
            if (typeof(IWarehouse).IsAssignableFrom(type))
                Locator.CurrentMutable.RegisterLazySingleton(() => {
                    var persist = (IPersist) Locator.CurrentMutable.GetService(typeof(IPersist));
                    try {
                        return persist.Load<IWarehouse>();
                    }
                    //File not exists yet
                    catch (FileNotFoundException) {
                        return Activator.CreateInstance(type);
                    }
                }, typeof(IWarehouse));
        }

        private static void RegisterProductService<TConcrete, TInterface>()
            where TConcrete : BaseProductService<TInterface> where TInterface : class, IProduct {
            Locator.CurrentMutable.RegisterLazySingleton(
                () => Activator.CreateInstance(typeof(TConcrete),
                    (IWarehouse) Locator.CurrentMutable.GetService(typeof(IWarehouse)),
                    (IPersist) Locator.CurrentMutable.GetService(typeof(IPersist))
                ), typeof(BaseProductService<TInterface>));
        }

        private static void RegisterService<TConcrete, TInterface>() where TConcrete : BaseService<TInterface>
            where TInterface : class {
            Locator.CurrentMutable.RegisterLazySingleton(
                () => Activator.CreateInstance(typeof(TConcrete),
                    (IWarehouse) Locator.CurrentMutable.GetService(typeof(IWarehouse)),
                    (IPersist) Locator.CurrentMutable.GetService(typeof(IPersist))
                ), typeof(BaseService<TInterface>));
        }

        private void Register<T>(Type type) {
            if (typeof(T).IsAssignableFrom(type))
                Locator.CurrentMutable.Register(() => Activator.CreateInstance(type), typeof(T));
        }

        private static void RegisterSingleton<T>(Type type) {
            if (typeof(T).IsAssignableFrom(type))
                Locator.CurrentMutable.RegisterLazySingleton(() => Activator.CreateInstance(type), typeof(T));
        }
    }
}