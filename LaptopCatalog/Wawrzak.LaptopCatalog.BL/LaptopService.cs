﻿using System;
using System.Linq;
using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.BL {
    public class LaptopService : BaseProductService<ILaptop> {
        private readonly IPersist persist;
        private readonly IWarehouse wh;

        public LaptopService(IWarehouse wh, IPersist persist) : base(wh.Laptops) {
            this.wh = wh;
            this.persist = persist;
        }

        public override void Add(ILaptop product) {
            Validate(product);
            base.Add(product);
        }

        public override void Put(ILaptop oldProduct, ILaptop newProduct) {
            Validate(newProduct);
            base.Put(oldProduct, newProduct);
        }

        public override void Validate(ILaptop laptop) {
            base.Validate(laptop);
            if (laptop.Chipset == null) throw new ArgumentNullException(nameof(laptop.Chipset));
            if (laptop.Cpu == null) throw new ArgumentNullException(nameof(laptop.Cpu));
            if (laptop.Disk == null || laptop.Disk.Count == 0)
                throw new ArgumentNullException(nameof(laptop.Disk));
            if (laptop.Display == null)
                throw new ArgumentNullException(nameof(laptop.Display));
            if (laptop.Gpu == null || laptop.Gpu.Count == 0 || laptop.Gpu.All(x => x.Product == null))
                throw new ArgumentNullException(nameof(laptop.Gpu));
            if (laptop.OperatingSystem == null)
                throw new ArgumentNullException(nameof(laptop.OperatingSystem));
            if (laptop.Price == default(decimal))
                throw new ArgumentNullException(nameof(laptop.Price));
            if (laptop.Ram == null || laptop.Ram.Count == 0 || laptop.Ram.All(x => x.Product == null))
                throw new ArgumentNullException(nameof(laptop.Ram));
        }

        public override void Save() {
            persist.Save(wh);
        }
    }
}