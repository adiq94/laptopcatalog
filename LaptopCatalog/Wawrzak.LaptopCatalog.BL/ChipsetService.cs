﻿using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.BL {
    public class ChipsetService : BaseProductService<IChipset> {
        private readonly IPersist persist;
        private readonly IWarehouse warehouse;

        public ChipsetService(IWarehouse warehouse, IPersist persist) : base(warehouse.Chipsets) {
            this.warehouse = warehouse;
            this.persist = persist;
        }

        public override void Save() {
            persist.Save(warehouse);
        }
    }
}