﻿using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.BL {
    public class DisplayService : BaseService<IDisplay> {
        private readonly IPersist persist;
        private readonly IWarehouse wh;

        public DisplayService(IWarehouse wh, IPersist persist) : base(wh.Displays) {
            this.wh = wh;
            this.persist = persist;
        }

        public override void Save() {
            persist.Save(wh);
        }
    }
}