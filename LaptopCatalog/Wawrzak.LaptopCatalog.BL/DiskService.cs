﻿using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.BL {
    public class DiskService : BaseProductService<IDisk> {
        private readonly IPersist persist;
        private readonly IWarehouse wh;

        public DiskService(IWarehouse wh, IPersist persist) : base(wh.Disks) {
            this.wh = wh;
            this.persist = persist;
        }

        public override void Save() {
            persist.Save(wh);
        }
    }
}