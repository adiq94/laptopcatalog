﻿using System;
using System.Collections.Generic;
using System.Linq;
using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.BL {
    public abstract class BaseService<T> : IService<T> where T : class {
        protected readonly HashSet<T> set;

        public BaseService(HashSet<T> set) {
            this.set = set;
        }

        public virtual void Add(T product) {
            Validate(product);
            set.Add(product);
            Save();
        }

        public virtual void Delete(T product) {
            set.Remove(product);
            Save();
        }

        public virtual IQueryable<T> Get() {
            return set.AsQueryable();
        }

        public virtual void Put(T oldProduct, T newProduct) {
            Validate(newProduct);
            Delete(oldProduct);
            Add(newProduct);
        }

        public virtual void Put(T product) {
            Put(product, product);
        }

        public virtual void Validate(T product) {
            if (product == null) throw new ArgumentNullException(nameof(product));
            if (product is IProducer p) {
                if(string.IsNullOrEmpty(p.Name)) throw new ArgumentNullException(nameof(p.Name));
            }
        }

        public virtual void Save() {
        }
    }
}