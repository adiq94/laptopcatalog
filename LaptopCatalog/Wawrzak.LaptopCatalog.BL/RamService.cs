﻿using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.BL {
    public class RamService : BaseProductService<IRam> {
        private readonly IPersist persist;
        private readonly IWarehouse wh;

        public RamService(IWarehouse wh, IPersist persist) : base(wh.Rams) {
            this.wh = wh;
            this.persist = persist;
        }

        public override void Save() {
            persist.Save(wh);
        }
    }
}