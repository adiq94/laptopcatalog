﻿using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.BL {
    public class CpuService : BaseProductService<ICpu> {
        private readonly IPersist persist;
        private readonly IWarehouse warehouse;

        public CpuService(IWarehouse warehouse, IPersist persist) : base(warehouse.CPUs) {
            this.warehouse = warehouse;
            this.persist = persist;
        }

        public override void Save() {
            persist.Save(warehouse);
        }
    }
}