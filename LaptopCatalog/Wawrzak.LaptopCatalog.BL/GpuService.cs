﻿using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.BL {
    public class GpuService : BaseProductService<IGpu> {
        private readonly IPersist persist;
        private readonly IWarehouse wh;

        public GpuService(IWarehouse wh, IPersist persist) : base(wh.Gpus) {
            this.wh = wh;
            this.persist = persist;
        }

        public override void Save() {
            persist.Save(wh);
        }
    }
}