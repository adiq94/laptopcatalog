﻿using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.BL {
    public class OSService : BaseProductService<IOperatingSystem> {
        private readonly IPersist persist;
        private readonly IWarehouse wh;

        public OSService(IWarehouse wh, IPersist persist) : base(wh.OSes) {
            this.wh = wh;
            this.persist = persist;
        }

        public override void Save() {
            persist.Save(wh);
        }
    }
}