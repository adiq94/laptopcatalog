﻿using System;
using System.Collections.Generic;
using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.BL {
    public abstract class BaseProductService<T> : BaseService<T> where T : class, IProduct {
        public BaseProductService(HashSet<T> set) : base(set) {
        }

        public override void Validate(T product) {
            base.Validate(product);
            if (string.IsNullOrEmpty(product.Name))
                throw new ArgumentNullException(nameof(product.Name));
            if (product.Producer == null)
                throw new ArgumentNullException(nameof(product.Producer));
        }
    }
}