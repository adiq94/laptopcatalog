﻿using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.BL {
    public class ProducerService : BaseService<IProducer> {
        private readonly IPersist persist;
        private readonly IWarehouse wh;

        public ProducerService(IWarehouse wh, IPersist persist) : base(wh.Producers) {
            this.wh = wh;
            this.persist = persist;
        }

        public override void Save() {
            persist.Save(wh);
        }
    }
}