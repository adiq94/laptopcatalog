﻿using System.IO;
using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.DAO {
    public class XmlPersist : IPersist {
        private readonly XmlSerializerWrapper serializer;

        public XmlPersist() {
            serializer = new XmlSerializerWrapper();
        }


        public void Save<T>(T model) where T : class {
            var serialized = serializer.Serialize(model);
            File.WriteAllText(typeof(T).Name, serialized);
        }

        public T Load<T>() where T : class {
            var model = File.ReadAllText(typeof(T).Name);
            return serializer.Deserialize<T>(model);
        }
    }
}