﻿using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.DAO {
    public class Gpu : IGpu {
        public IProducer Producer { get; set; }
        public string Name { get; set; }
        public decimal Frequency { get; set; }
        public IMemory Memory { get; set; }
    }
}