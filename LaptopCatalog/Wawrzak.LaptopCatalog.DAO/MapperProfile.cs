﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.DAO {
    public class MapperProfile : Profile {
        public MapperProfile() {
            CreateMap<Chipset, ChipsetXml>().ReverseMap();
            CreateMap<Cpu, CpuXml>().ReverseMap();
            CreateMap<Disk, DiskXml>().ReverseMap();
            CreateMap<Gpu, GpuXml>().ReverseMap();
            CreateMap<Laptop, LaptopXml>().ReverseMap();
            CreateMap<Memory, MemoryXml>().ReverseMap();
            CreateMap<OperatingSystem, OperatingSystemXml>().ReverseMap();
            CreateMap<Ram, RamXml>().ReverseMap();
            CreateMap<Warehouse, WarehouseXml>().ReverseMap();
            CreateMap<Slot<Gpu>, SlotXml<Gpu>>().ReverseMap();
            CreateMap<Slot<Cpu>, SlotXml<CpuXml>>().ReverseMap();
            CreateMap<Slot<Disk>, SlotXml<DiskXml>>().ReverseMap();
            CreateMap<HashSet<ChipsetXml>, HashSet<IChipset>>()
                .ConstructUsing((x, ctx) =>
                    new HashSet<IChipset>(x.Select(ctx.Mapper.Map<IChipset>), new ProductComparer()));
            CreateMap<HashSet<CpuXml>, HashSet<ICpu>>()
                .ConstructUsing((x, ctx) => new HashSet<ICpu>(x.Select(ctx.Mapper.Map<ICpu>), new ProductComparer()));
            CreateMap<HashSet<DiskXml>, HashSet<IDisk>>()
                .ConstructUsing((x, ctx) => new HashSet<IDisk>(x.Select(ctx.Mapper.Map<IDisk>), new ProductComparer()));
            CreateMap<HashSet<Display>, HashSet<IDisplay>>()
                .ConstructUsing((x, ctx) =>
                    new HashSet<IDisplay>(x.Select(ctx.Mapper.Map<IDisplay>), new DisplayComparer()));
            CreateMap<HashSet<LaptopXml>, HashSet<ILaptop>>()
                .ConstructUsing((x, ctx) =>
                    new HashSet<ILaptop>(x.Select(ctx.Mapper.Map<ILaptop>), new ProductComparer()));
            CreateMap<HashSet<OperatingSystemXml>, HashSet<IOperatingSystem>>()
                .ConstructUsing((x, ctx) =>
                    new HashSet<IOperatingSystem>(x.Select(ctx.Mapper.Map<IOperatingSystem>), new ProductComparer()));
            CreateMap<HashSet<Producer>, HashSet<IProducer>>()
                .ConstructUsing((x, ctx) =>
                    new HashSet<IProducer>(x.Select(ctx.Mapper.Map<IProducer>), new ProducerComparer()));
            CreateMap<HashSet<RamXml>, HashSet<IRam>>()
                .ConstructUsing((x, ctx) => new HashSet<IRam>(x.Select(ctx.Mapper.Map<IRam>), new ProductComparer()));
            CreateMap<HashSet<GpuXml>, HashSet<IGpu>>()
                .ConstructUsing((x, ctx) => new HashSet<IGpu>(x.Select(ctx.Mapper.Map<IGpu>), new ProductComparer()));
        }
    }
}