﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;
using AutoMapper;
using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.DAO {
    public class XmlSerializerWrapper {
        public XmlSerializerWrapper() {
            Mapper.Initialize(cfg => { cfg.AddProfile<MapperProfile>(); });
            Mapper.AssertConfigurationIsValid();
        }

        public string Serialize<T>(T model) {
            if (typeof(T) == typeof(IWarehouse)) {
                var serializer = new XmlSerializer(typeof(WarehouseXml));
                var wh = model as Warehouse;
                var mappedModel = Mapper.Map<WarehouseXml>(wh);
                using (var sww = new StringWriter()) {
                    using (var writer = XmlWriter.Create(sww)) {
                        serializer.Serialize(writer, mappedModel);
                        return sww.ToString();
                    }
                }
            }
            else {
                var serializer = new XmlSerializer(model.GetType());

                using (var sww = new StringWriter()) {
                    using (var writer = XmlWriter.Create(sww)) {
                        serializer.Serialize(writer, model);
                        return sww.ToString();
                    }
                }
            }
        }

        public T Deserialize<T>(string model) where T : class {
            if (typeof(T) == typeof(IWarehouse)) {
                var serializer = new XmlSerializer(typeof(WarehouseXml));
                var stringReader = new StringReader(model);
                var warehouseXml = (WarehouseXml) serializer.Deserialize(stringReader);
                return Mapper.Map<WarehouseXml, Warehouse>(warehouseXml) as T;
            }
            else {
                var serializer = new XmlSerializer(typeof(T));
                var stringReader = new StringReader(model);
                return (T) serializer.Deserialize(stringReader);
            }
        }
    }
}