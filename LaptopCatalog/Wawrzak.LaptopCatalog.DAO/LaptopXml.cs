﻿using System.Collections.Generic;
using Wawrzak.LaptopCatalog.Core;

namespace Wawrzak.LaptopCatalog.DAO {
    public class LaptopXml {
        public Display Display { get; set; }
        public CpuXml Cpu { get; set; }
        public HashSet<SlotXml<RamXml>> Ram { get; set; }
        public HashSet<SlotXml<GpuXml>> Gpu { get; set; }
        public HashSet<SlotXml<DiskXml>> Disk { get; set; }
        public ChipsetXml Chipset { get; set; }
        public Producer Producer { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public Currency Currency { get; set; }
        public OperatingSystemXml OperatingSystem { get; set; }
    }
}