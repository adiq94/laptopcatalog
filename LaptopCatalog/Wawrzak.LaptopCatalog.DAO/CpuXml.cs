﻿namespace Wawrzak.LaptopCatalog.DAO {
    public class CpuXml {
        public Producer Producer { get; set; }
        public string Name { get; set; }
        public decimal Frequency { get; set; }
    }
}