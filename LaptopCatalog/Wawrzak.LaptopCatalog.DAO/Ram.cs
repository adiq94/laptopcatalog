﻿using Wawrzak.LaptopCatalog.Core;
using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.DAO {
    public class Ram : IRam {
        public decimal Frequency { get; set; }
        public decimal Value { get; set; }
        public SizeUnit Unit { get; set; }
        public IProducer Producer { get; set; }
        public string Name { get; set; }
    }
}