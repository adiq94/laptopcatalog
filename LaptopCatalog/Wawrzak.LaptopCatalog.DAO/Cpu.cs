﻿using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.DAO {
    public class Cpu : ICpu {
        public IProducer Producer { get; set; }
        public string Name { get; set; }
        public decimal Frequency { get; set; }
    }
}