﻿using Wawrzak.LaptopCatalog.Core;

namespace Wawrzak.LaptopCatalog.DAO {
    public class DiskXml {
        public DiskType Type { get; set; }
        public Producer Producer { get; set; }
        public string Name { get; set; }
        public decimal Value { get; set; }
        public SizeUnit Unit { get; set; }
    }
}