﻿using System;
using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.DAO {
    [Serializable]
    public class Producer : IProducer {
        public string Name { get; set; }
    }
}