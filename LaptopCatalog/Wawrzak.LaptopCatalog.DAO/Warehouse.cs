﻿using System.Collections.Generic;
using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.DAO {
    public class Warehouse : IWarehouse {
        public Warehouse() {
            Chipsets = new HashSet<IChipset>(new ProductComparer());
            CPUs = new HashSet<ICpu>(new ProductComparer());
            Disks = new HashSet<IDisk>(new ProductComparer());
            Displays = new HashSet<IDisplay>(new DisplayComparer());
            Laptops = new HashSet<ILaptop>(new ProductComparer());
            OSes = new HashSet<IOperatingSystem>(new ProductComparer());
            Producers = new HashSet<IProducer>(new ProducerComparer());
            Rams = new HashSet<IRam>(new ProductComparer());
            Gpus = new HashSet<IGpu>(new ProductComparer());
        }

        public HashSet<IChipset> Chipsets { get; set; }
        public HashSet<ICpu> CPUs { get; set; }
        public HashSet<IDisk> Disks { get; set; }
        public HashSet<IDisplay> Displays { get; set; }
        public HashSet<ILaptop> Laptops { get; set; }
        public HashSet<IOperatingSystem> OSes { get; set; }
        public HashSet<IProducer> Producers { get; set; }
        public HashSet<IRam> Rams { get; set; }
        public HashSet<IGpu> Gpus { get; set; }
    }
}