﻿using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.DAO {
    public class Chipset : IChipset {
        public IProducer Producer { get; set; }
        public string Name { get; set; }
    }
}