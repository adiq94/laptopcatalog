﻿using Wawrzak.LaptopCatalog.Core;

namespace Wawrzak.LaptopCatalog.DAO {
    public class RamXml {
        public decimal Frequency { get; set; }
        public decimal Value { get; set; }
        public SizeUnit Unit { get; set; }
        public Producer Producer { get; set; }
        public string Name { get; set; }
    }
}