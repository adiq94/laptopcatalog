﻿using Wawrzak.LaptopCatalog.Core;
using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.DAO {
    public class Disk : IDisk {
        public DiskType Type { get; set; }
        public IProducer Producer { get; set; }
        public string Name { get; set; }
        public decimal Value { get; set; }
        public SizeUnit Unit { get; set; }
    }
}