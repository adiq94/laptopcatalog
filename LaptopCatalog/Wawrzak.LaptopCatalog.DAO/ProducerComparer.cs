﻿using System.Collections.Generic;
using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.DAO {
    public class ProducerComparer : EqualityComparer<IProducer> {
        public override bool Equals(IProducer x, IProducer y) {
            return x?.Name == y?.Name;
        }

        public override int GetHashCode(IProducer obj) {
            return obj?.Name?.GetHashCode() ?? 0;
        }
    }
}