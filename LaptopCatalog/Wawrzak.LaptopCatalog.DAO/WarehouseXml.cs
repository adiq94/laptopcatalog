﻿using System.Collections.Generic;

namespace Wawrzak.LaptopCatalog.DAO {
    public class WarehouseXml {
        public HashSet<ChipsetXml> Chipsets { get; set; }
        public HashSet<CpuXml> CPUs { get; set; }
        public HashSet<DiskXml> Disks { get; set; }
        public HashSet<Display> Displays { get; set; }
        public HashSet<LaptopXml> Laptops { get; set; }
        public HashSet<OperatingSystemXml> OSes { get; set; }
        public HashSet<Producer> Producers { get; set; }
        public HashSet<RamXml> Rams { get; set; }
        public HashSet<GpuXml> Gpus { get; set; }
    }
}