﻿using Wawrzak.LaptopCatalog.Core;
using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.DAO {
    public class Display : IDisplay {
        public DisplayType Type { get; set; }
        public DisplayCoatingType CoatingType { get; set; }
        public decimal Size { get; set; }
        public int ResolutionHorizontal { get; set; }
        public int ResolutionVertical { get; set; }
    }
}