﻿namespace Wawrzak.LaptopCatalog.DAO {
    public class GpuXml {
        public Producer Producer { get; set; }
        public string Name { get; set; }
        public decimal Frequency { get; set; }
        public MemoryXml Memory { get; set; }
    }
}