﻿using System.Collections.Generic;
using Wawrzak.LaptopCatalog.Core;
using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.DAO {
    public class Laptop : ILaptop {
        public IDisplay Display { get; set; }
        public ICpu Cpu { get; set; }
        public ICollection<ISlot<IRam>> Ram { get; set; }
        public ICollection<ISlot<IGpu>> Gpu { get; set; }
        public ICollection<ISlot<IDisk>> Disk { get; set; }
        public IChipset Chipset { get; set; }
        public IProducer Producer { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public Currency Currency { get; set; }
        public IOperatingSystem OperatingSystem { get; set; }
    }
}