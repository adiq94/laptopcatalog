﻿using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.DAO {
    public class Slot<T> : ISlot<T> where T : IProduct {
        public T Product { get; set; }
    }
}