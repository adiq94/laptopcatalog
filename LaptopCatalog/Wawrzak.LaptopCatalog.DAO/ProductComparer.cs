﻿using System.Collections.Generic;
using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.DAO {
    public class ProductComparer : EqualityComparer<IProduct> {
        public override bool Equals(IProduct x, IProduct y) {
            return x?.Name == y?.Name && x?.Producer?.Name == y?.Producer?.Name;
        }

        public override int GetHashCode(IProduct obj) {
            unchecked {
                var result = obj?.Name.GetHashCode() * obj?.Producer.Name.GetHashCode();
                return result ?? 0;
            }
        }
    }
}