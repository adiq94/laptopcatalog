﻿using System.Collections.Generic;
using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.DAO {
    public class DisplayComparer : EqualityComparer<IDisplay> {
        public override bool Equals(IDisplay x, IDisplay y) {
            return x != null && x.Type == y.Type && x.CoatingType == y.CoatingType && x.Size == y.Size &&
                   x.ResolutionHorizontal == y.ResolutionHorizontal && x.ResolutionVertical == y.ResolutionVertical;
        }

        public override int GetHashCode(IDisplay obj) {
            unchecked {
                if (obj != null)
                    return obj.CoatingType.GetHashCode() * obj.Size.GetHashCode() *
                           obj.ResolutionHorizontal.GetHashCode()
                           * obj.ResolutionVertical.GetHashCode();
                return 0;
            }
        }
    }
}