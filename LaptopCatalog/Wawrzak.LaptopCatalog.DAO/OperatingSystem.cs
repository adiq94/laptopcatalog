﻿using Wawrzak.LaptopCatalog.Interfaces;

namespace Wawrzak.LaptopCatalog.DAO {
    public class OperatingSystem : IOperatingSystem {
        public IProducer Producer { get; set; }
        public string Name { get; set; }
    }
}