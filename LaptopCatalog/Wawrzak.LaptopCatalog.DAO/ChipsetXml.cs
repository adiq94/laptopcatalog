﻿namespace Wawrzak.LaptopCatalog.DAO {
    public class ChipsetXml {
        public Producer Producer { get; set; }
        public string Name { get; set; }
    }
}