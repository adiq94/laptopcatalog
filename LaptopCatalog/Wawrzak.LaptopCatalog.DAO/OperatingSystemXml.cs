﻿namespace Wawrzak.LaptopCatalog.DAO {
    public class OperatingSystemXml {
        public Producer Producer { get; set; }
        public string Name { get; set; }
    }
}