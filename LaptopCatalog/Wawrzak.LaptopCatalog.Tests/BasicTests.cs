﻿using System.Collections.Generic;
using System.Linq;
using Wawrzak.LaptopCatalog.BL;
using Wawrzak.LaptopCatalog.Core;
using Wawrzak.LaptopCatalog.DAO;
using Wawrzak.LaptopCatalog.Interfaces;
using Xunit;

namespace Wawrzak.LaptopCatalog.Tests {
    public class BasicTests {
        [Fact]
        public void CreateLaptop() {
            var laptop = new Laptop();
            Assert.NotNull(laptop);
        }

        [Fact]
        public void ServiceAdd() {
            var warehouse = new Warehouse();
            var service = new ChipsetService(warehouse, new XmlPersist());
            var producer = new Producer {Name = "Producer"};
            var chipset = new Chipset {Name = "SomeChipset", Producer = producer};
            service.Add(chipset);
            Assert.Equal(chipset, service.Get().Where(x => x == chipset).First());
        }

        [Fact]
        public void ServiceAdd_IgnoreDuplicate() {
            var warehouse = new Warehouse();
            var service = new ChipsetService(warehouse, new XmlPersist());
            var producer = new Producer {Name = "Producer"};
            var chipset = new Chipset {Name = "SomeChipset", Producer = producer};
            service.Add(chipset);
            var chipset2 = new Chipset {Name = "SomeChipset", Producer = producer};
            service.Add(chipset2);
            Assert.Equal(1, service.Get().Count());
        }

        [Fact]
        public void ServiceAdd_Producer_IgnoreDuplicate() {
            var warehouse = new Warehouse();
            var service = new ProducerService(warehouse, new XmlPersist());
            var producer = new Producer {Name = "Producer"};
            var producer2 = new Producer {Name = "Producer"};
            service.Add(producer);
            service.Add(producer2);
            Assert.Equal(1, service.Get().Count());
        }

        [Fact]
        public void ServiceAddLaptop_IntegrationTest() {
            var warehouse = new Warehouse();
            var persist = new XmlPersist();
            var service = new ChipsetService(warehouse, persist);
            var cpuService = new CpuService(warehouse, persist);
            var ramService = new RamService(warehouse, persist);
            var laptopService = new LaptopService(warehouse, persist);
            var diskService = new DiskService(warehouse, persist);
            var displayService = new DisplayService(warehouse, persist);
            var gpuService = new GpuService(warehouse, persist);
            var osService = new OSService(warehouse, persist);
            var producer = new Producer {Name = "Producer"};
            var chipset = new Chipset {Name = "SomeChipset", Producer = producer};
            var ram = new Ram {Frequency = 2400, Name = "Ram", Producer = producer, Unit = SizeUnit.MB, Value = 4096};
            var cpu = new Cpu {Frequency = 3200, Name = "i5 6600k", Producer = producer};
            var disk = new Disk
                {Name = "Disk", Producer = producer, Type = DiskType.HDD, Unit = SizeUnit.GB, Value = 500};
            var display = new Display {
                CoatingType = DisplayCoatingType.Matte, ResolutionHorizontal = 1920, ResolutionVertical = 1080,
                Size = 14, Type = DisplayType.IPS
            };
            var gpu = new Gpu {
                Name = "Geforce RTX 2070",
                Frequency = 4000,
                Memory = new Memory
                    {Frequency = 2400, Name = "NoName", Producer = producer, Unit = SizeUnit.MB, Value = 1024},
                Producer = producer
            };
            var os = new OperatingSystem {Name = "Windows 10", Producer = producer};
            var laptop = new Laptop {
                Name = "Laptopek",
                Chipset = chipset,
                Cpu = cpu,
                Disk = new HashSet<ISlot<IDisk>> {new Slot<IDisk> {Product = disk}},
                Display = display,
                Gpu = new List<ISlot<IGpu>> {new Slot<IGpu> {Product = gpu}},
                OperatingSystem = os,
                Ram = new HashSet<ISlot<IRam>> {new Slot<IRam> {Product = ram}},
                Price = 4000,
                Producer = producer
            };
            service.Add(chipset);
            ramService.Add(ram);
            cpuService.Add(cpu);
            laptopService.Add(laptop);
            diskService.Add(disk);
            displayService.Add(display);
            gpuService.Add(gpu);
            osService.Add(os);
            Assert.Equal(chipset, service.Get().Where(x => x == chipset).First());
            var saved = persist.Load<IWarehouse>();
        }

        [Fact]
        public void ServiceDelete() {
            var warehouse = new Warehouse();
            var service = new ChipsetService(warehouse, new XmlPersist());
            var producer = new Producer {Name = "Producer"};
            var chipset = new Chipset {Name = "SomeChipset", Producer = producer};
            service.Add(chipset);
            Assert.Equal(chipset, service.Get().Where(x => x == chipset).First());
            service.Delete(chipset);
            Assert.Empty(service.Get());
        }

        [Fact]
        public void ServicePut() {
            var warehouse = new Warehouse();
            var service = new ChipsetService(warehouse, new XmlPersist());
            var producer = new Producer {Name = "Producer"};
            var chipset = new Chipset {Name = "SomeChipset", Producer = producer};
            service.Put(chipset, chipset);
            Assert.Equal(chipset, service.Get().Where(x => x == chipset).First());
            Assert.Equal(1, service.Get().Count());
        }

        [Fact]
        public void ServicePut_Replace() {
            var warehouse = new Warehouse();
            var service = new ChipsetService(warehouse, new XmlPersist());
            var producer = new Producer {Name = "Producer"};
            var chipset = new Chipset {Name = "SomeChipset", Producer = producer};
            service.Put(chipset, chipset);
            var chipset2 = new Chipset {Name = "SomeChipset2", Producer = producer};
            service.Put(chipset, chipset2);
            Assert.Equal(chipset2, service.Get().Where(x => x == chipset2).First());
            Assert.Equal(1, service.Get().Count());
            Assert.Equal("SomeChipset2", service.Get().First().Name);
        }
    }
}