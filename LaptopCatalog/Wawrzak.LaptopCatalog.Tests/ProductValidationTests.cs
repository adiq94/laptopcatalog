﻿using System;
using System.Linq;
using Wawrzak.LaptopCatalog.BL;
using Wawrzak.LaptopCatalog.DAO;
using Xunit;

namespace Wawrzak.LaptopCatalog.Tests {
    public class ProductValidationTests {
        [Fact]
        public void When_correct_Not_Throw_Exception() {
            var service = new CpuService(new Warehouse(), new XmlPersist());
            service.Add(new Cpu {
                Frequency = 2400,
                Name = "Somename",
                Producer = new Producer {
                    Name = "Some producer"
                }
            });
            Assert.Equal(1, service.Get().Count());
        }

        [Fact]
        public void When_empty_name_Throw_Exception() {
            var service = new CpuService(new Warehouse(), new XmlPersist());
            Assert.Throws<ArgumentNullException>(() => service.Add(new Cpu()));
        }

        [Fact]
        public void When_empty_producer_Throw_Exception() {
            var service = new CpuService(new Warehouse(), new XmlPersist());
            Assert.Throws<ArgumentNullException>(() => service.Add(new Cpu {
                Frequency = 2400,
                Name = "Somename"
            }));
        }
    }
}