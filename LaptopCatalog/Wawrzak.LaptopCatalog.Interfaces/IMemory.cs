﻿namespace Wawrzak.LaptopCatalog.Interfaces {
    public interface IMemory : IFrequency, ISize, IProduct {
    }
}