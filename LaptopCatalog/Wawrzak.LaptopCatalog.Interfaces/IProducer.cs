﻿namespace Wawrzak.LaptopCatalog.Interfaces {
    public interface IProducer {
        string Name { get; set; }
    }
}