﻿using Wawrzak.LaptopCatalog.Core;

namespace Wawrzak.LaptopCatalog.Interfaces {
    public interface ISize {
        decimal Value { get; set; }
        SizeUnit Unit { get; set; }
    }
}