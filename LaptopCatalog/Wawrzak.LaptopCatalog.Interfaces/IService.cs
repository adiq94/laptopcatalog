﻿using System.Linq;

namespace Wawrzak.LaptopCatalog.Interfaces {
    public interface IService<T> {
        IQueryable<T> Get();
        void Add(T product);
        void Put(T oldProduct, T newProduct);
        void Delete(T product);
    }
}