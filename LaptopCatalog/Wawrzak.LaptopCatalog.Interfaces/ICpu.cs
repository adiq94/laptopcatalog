﻿namespace Wawrzak.LaptopCatalog.Interfaces {
    public interface ICpu : IProduct, IFrequency {
    }
}