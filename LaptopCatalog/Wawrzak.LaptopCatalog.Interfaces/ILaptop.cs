﻿using System.Collections.Generic;

namespace Wawrzak.LaptopCatalog.Interfaces {
    public interface ILaptop : IProduct, IPrice {
        IDisplay Display { get; set; }
        ICpu Cpu { get; set; }
        ICollection<ISlot<IRam>> Ram { get; set; }
        ICollection<ISlot<IGpu>> Gpu { get; set; }
        ICollection<ISlot<IDisk>> Disk { get; set; }
        IChipset Chipset { get; set; }
        IOperatingSystem OperatingSystem { get; set; }
    }
}