﻿namespace Wawrzak.LaptopCatalog.Interfaces {
    public interface IProduct {
        IProducer Producer { get; set; }
        string Name { get; set; }
    }
}