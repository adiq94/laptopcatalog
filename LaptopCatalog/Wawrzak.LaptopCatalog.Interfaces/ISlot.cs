﻿namespace Wawrzak.LaptopCatalog.Interfaces {
    public interface ISlot<T> where T : IProduct {
        T Product { get; set; }
    }
}