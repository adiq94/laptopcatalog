﻿namespace Wawrzak.LaptopCatalog.Interfaces {
    public interface IFrequency {
        decimal Frequency { get; set; }
    }
}