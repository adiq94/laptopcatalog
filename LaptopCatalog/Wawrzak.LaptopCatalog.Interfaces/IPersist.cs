﻿namespace Wawrzak.LaptopCatalog.Interfaces {
    public interface IPersist {
        void Save<T>(T model) where T : class;
        T Load<T>() where T : class;
    }
}