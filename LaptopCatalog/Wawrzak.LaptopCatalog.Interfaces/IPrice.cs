﻿using Wawrzak.LaptopCatalog.Core;

namespace Wawrzak.LaptopCatalog.Interfaces {
    public interface IPrice {
        decimal Price { get; set; }
        Currency Currency { get; set; }
    }
}