﻿using System.Collections.Generic;

namespace Wawrzak.LaptopCatalog.Interfaces {
    public interface IWarehouse {
        HashSet<IChipset> Chipsets { get; }
        HashSet<ICpu> CPUs { get; }
        HashSet<IDisk> Disks { get; }
        HashSet<IDisplay> Displays { get; }
        HashSet<ILaptop> Laptops { get; }
        HashSet<IOperatingSystem> OSes { get; }
        HashSet<IProducer> Producers { get; }
        HashSet<IRam> Rams { get; }
        HashSet<IGpu> Gpus { get; }
    }
}