﻿namespace Wawrzak.LaptopCatalog.Interfaces {
    public interface IGpu : IProduct, IFrequency {
        IMemory Memory { get; set; }
    }
}