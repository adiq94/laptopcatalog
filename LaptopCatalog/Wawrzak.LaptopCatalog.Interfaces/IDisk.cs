﻿using Wawrzak.LaptopCatalog.Core;

namespace Wawrzak.LaptopCatalog.Interfaces {
    public interface IDisk : IProduct, ISize {
        DiskType Type { get; set; }
    }
}