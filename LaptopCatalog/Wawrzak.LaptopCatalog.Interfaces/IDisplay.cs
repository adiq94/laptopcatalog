﻿using Wawrzak.LaptopCatalog.Core;

namespace Wawrzak.LaptopCatalog.Interfaces {
    public interface IDisplay {
        DisplayType Type { get; set; }
        DisplayCoatingType CoatingType { get; set; }
        decimal Size { get; set; }
        int ResolutionHorizontal { get; set; }
        int ResolutionVertical { get; set; }
    }
}